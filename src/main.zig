const std = @import("std");

comptime {
    const tsize = 16;
    const BF = Machine(tsize);
    var state = State(BF).create(BF {
        .codeptr = 0,

        .tape = [_]u8{0} ** tsize,
        .pointer = 0,
        .callstack = Stack(8, usize).init(0),
        .buffer = Stack(64, u8).init(0)
    });
    const file = @embedFile("default.bf");
    @setEvalBranchQuota(10000);
    inline while (state.value.codeptr < file.len) {
        state = parse(BF, state, file[state.value.codeptr]);
    }
    @compileLog(state.value.buffer.items);
}

fn parse(comptime T: type, comptime state: State(T), comptime char: u8) State(T) {
    var tape = state.value.tape;
    var pointer = state.value.pointer;
    var callstack = state.value.callstack;
    var buffer = state.value.buffer;
    var codeptr = state.value.codeptr;
    
    switch (char) {
        '+' => { tape[pointer] += 1;      codeptr += 1; },
        '-' => { tape[pointer] -= 1;      codeptr += 1; },
        '>' => { pointer += 1;            codeptr += 1; },
        '<' => { pointer -= 1;            codeptr += 1; },
        '[' => { callstack.push(codeptr); codeptr += 1; },
        ']' => {
            if (tape[pointer] != 0) {
                codeptr = callstack.pop();
            } else {
                codeptr += 1;
            }
        },
        '.' => { buffer.push(tape[pointer]); codeptr += 1; },
        else => { codeptr += 1; }
    }

    return State(T) {
        .old_state = &state,
        .value = T {
            .codeptr = codeptr,

            .tape = tape,
            .pointer = pointer,
            .callstack = callstack,
            .buffer = buffer
        }
    };
}

pub fn Machine(comptime s: usize) type {
    return struct {
        const Self = @This();
        codeptr: usize,
        tape: [s]u8,
        pointer: usize,
        callstack: Stack(8, usize),
        buffer: Stack(64, u8)
    };
}
pub fn Stack(comptime s: usize, comptime T: type) type {
    return struct {
        const Self = @This();
        items: [s]T,
        pointer: usize,

        pub fn init(comptime v: T) Self {
            return Self {
                .items = [_]T {v} ** s,
                .pointer = 0
            };
        }

        pub fn push(self: *Self, value: T) void {
            self.pointer += 1;
            self.items[self.pointer] = value;
        }

        pub fn pop(self: *Self) T {
            var ret = self.items[self.pointer];
            self.pointer -= 1;
            return ret;
        }
    };
}
pub fn State(comptime T: type) type {
    return struct {
        const Self = @This();
        old_state: ?*const Self,
        value: T,

        pub fn create(comptime value: T) Self {
            return Self {
                .old_state = null,
                .value = value
            };
        }
    };
}

pub fn main() void {}
